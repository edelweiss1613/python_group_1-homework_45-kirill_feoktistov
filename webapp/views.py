from django.views.generic import DetailView, CreateView, UpdateView, View, DeleteView, ListView
from django.urls import reverse, reverse_lazy
from django.http import HttpResponseRedirect, HttpResponseForbidden
from webapp.models import Food, Order, OrderFood
from webapp.forms import FoodForm, OrderForm, OrderFoodForm
from django.shortcuts import get_object_or_404, redirect
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin


class MainPageView(LoginRequiredMixin, ListView):

    model = Order
    template_name = 'index.html'
    ordering = ['status']


class FoodListView(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    permission_required = 'webapp.view_food'
    model = Food
    template_name = 'food_list.html'


class FoodDetailView(LoginRequiredMixin, PermissionRequiredMixin, DetailView):
    permission_required = 'webapp.view_food'
    model = Food
    template_name = 'food_detail.html'


class FoodUpdateView(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    permission_required = 'webapp.change_food'
    model = Food
    template_name = 'food_update.html'
    form_class = FoodForm

    def get_success_url(self):
        return reverse('webapp:food_detail', kwargs={'pk': self.object.pk})


class FoodDeleteView(LoginRequiredMixin, PermissionRequiredMixin, DeleteView):
    permission_required = 'webapp.delete_food'
    model = Food
    success_url = reverse_lazy('webapp:food_list')


class FoodCreateView(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    permission_required = 'webapp.add_food'
    model = Food
    template_name = 'food_create.html'
    form_class = FoodForm

    def get_success_url(self):
        return reverse('webapp:food_detail', kwargs={'pk': self.object.pk})


class OrderDetailView(LoginRequiredMixin, PermissionRequiredMixin, DetailView):
    permission_required = 'webapp.view_order'
    model = Order
    template_name = 'order_detail.html'


class OrderUpdateView(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    permission_required = 'webapp.change_order'
    model = Order
    template_name = 'order_update.html'
    form_class = OrderForm

    def get_success_url(self):
        return reverse('webapp:order_detail', kwargs={'pk': self.object.pk})


class OrderDeliverView(LoginRequiredMixin, PermissionRequiredMixin, View):
    permission_required = 'webapp.can_take_order'

    def get(self, *args, **kwargs):
            order = get_object_or_404(Order, pk=self.kwargs['pk'])
            print(order.status)
            if order.status == 'preparing':
                order.status = 'on_way'
                order.courier = self.request.user
            elif order.status == 'on_way' and order.courier == self.request.user:
                order.status = 'delivered'
                order.courier = self.request.user
            order.save()
            return redirect('webapp:index')


# классовое на базе DeleteView с выводом страницы подтверждения
class OrderRejectView(LoginRequiredMixin, PermissionRequiredMixin, DeleteView):
    permission_required = 'webapp.delete_order'
    model = Order
    template_name = 'order_cancel.html'

    def get_success_url(self):
        return reverse('webapp:index')

    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        success_url = self.get_success_url()

        self.object.status = 'canceled'
        self.object.save()
        return HttpResponseRedirect(success_url)


# Представления для создания заказа
class OrderCreateView(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    permission_required = 'webapp.add_order'
    model = Order
    template_name = 'order_create.html'
    form_class = OrderForm

    def get_success_url(self):
        return reverse('webapp:order_detail', kwargs={'pk': self.object.pk})

    def form_valid(self, form):
        form.instance.operator = self.request.user
        form.save()
        return super(OrderCreateView, self).form_valid(form)



# ... и для добавления блюд в заказ
class OrderFoodCreateView(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    permission_required = 'webapp.add_orderfood'
    model = OrderFood
    form_class = OrderFoodForm
    template_name = 'order_food_create.html'

    def get_success_url(self):
        return reverse('webapp:order_detail', kwargs={'pk': self.object.order.pk})

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['order'] = Order.objects.get(pk=self.kwargs.get('pk'))
        return context

    def form_valid(self, form):
        form.instance.order = get_object_or_404(Order, pk=self.kwargs['pk'])
        form.save()
        return super(OrderFoodCreateView, self).form_valid(form)


class OrderFoodDeleteView(LoginRequiredMixin, PermissionRequiredMixin, DeleteView):
    permission_required = 'webapp.delete_orderfood'
    model = OrderFood
    template_name = 'order_food_delete.html'

    def get_success_url(self):
        return reverse('webapp:order_detail', kwargs={'pk': self.object.order.pk})

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['order'] = Order.objects.get(pk=self.kwargs.get('pk'))
        context['food'] = OrderFood.objects.get(pk=self.kwargs.get('food_pk'))
        return context

    def delete(self, request, *args, **kwargs):
        self.object = OrderFood.objects.get(pk=self.kwargs.get('food_pk'))
        success_url = self.get_success_url()
        self.object.delete()
        return HttpResponseRedirect(success_url)

