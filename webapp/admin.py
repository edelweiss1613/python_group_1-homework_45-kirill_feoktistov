from django.contrib import admin
from django.contrib.auth.models import User
from django.contrib.auth.admin import UserAdmin
from webapp.models import Employee, Food, Order, OrderFood


class EmployeeInline(admin.StackedInline):
    model = Employee


class EmployeeAdmin(UserAdmin):
    inlines = [EmployeeInline]


class OrderFoodsInline(admin.TabularInline):
    model = OrderFood
    fields = ['food', 'amount']


class OrderAdmin(admin.ModelAdmin):
    model = Order
    inlines = [OrderFoodsInline]
    list_display = ('contact_phone', 'contact_name', 'status', 'courier', 'operator')
    # fields = ['contact_phone', 'contact_name', 'status']


admin.site.unregister(User)
admin.site.register(User, EmployeeAdmin)
admin.site.register(Food)
admin.site.register(Order, OrderAdmin)
